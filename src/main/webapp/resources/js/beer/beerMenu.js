$(document).on('pageinit', '#beerMenu', function(event, ui){  
	var silentScroll = $.mobile.silentScroll;
    $.mobile.silentScroll = function( ypos ) {
	  if ( $.type( ypos ) !== "number" ) {
	      // FIX : prevent auto scroll to top after page load
	      return;
	  } else {
	      silentScroll.apply(this, arguments);
	  }
    }
});



$(document).on('pagebeforecreate', '#beerMenu', function(event, ui){  
	contextPath = $("#pageContextPath").attr("data") !== undefined ? $("#pageContextPath").attr("data") + '/' : '/';
	
    window.location.hash = '';

    var url = contextPath + 'beer/findAllInStock',
        mode = '';        
    
    $.ajax({
    	type: "GET",
        url: url + mode,
        dataType: "json",
        async: true,
        success: function (result) {
            ajax.parseJSONP(result);
        },
        error: function (request, sts, error) {
        	console.log(error + " : " + sts);
            console.log('Network error has occurred please try again!');
        }
    });         
});

$(document).on('vclick', '#beerList li a', function(e){  
	var idBeer = $(this).attr('data-id');
	//    $.mobile.changePage('beerShow.xhtml', { dataUrl : "beerShow?idBeer=" + idBeer, data : { 'idBeer' : idBeer}, reloadPage : true, changeHash : true, reverse : true, allowSamePageTransition : true });
	$.mobile.pageContainer.pagecontainer("change", "beerShow.xhtml?idBeer=" + idBeer, {reloadPage : true, changeHash : true } );
});

beerList = null;

var ajax = {  
    parseJSONP:function(result){
    	beerList = result;
        $.each(result, function(i, row) {
        	var formatter = new Intl.NumberFormat("pt-BR",{
        		minimumFractionDigits: 2
    		});
        	priceValue = formatter.format(row.price);
            $('#beerList').append('<li><a href="" data-id="' + row.id + '"><img class="img-list" src="' + contextPath + 'images/' +row.id +'"/><h3><b>' + row.name + '</b></h3><b style="color:#FF3300; font-size: 1em">R$ ' + priceValue + '</b><p>' + row.style.style + '</p><p>' + row.volume + 'ml</p></a></li>');
        });
        $('#beerList').listview().listview('refresh');
    }
};

$(document).on("pagebeforecreate","#beerShow",function(event, ui){
	contextPath = $("#pageContextPath").attr("data") !== undefined ? $("#pageContextPath").attr("data") + '/' : '/';
	var parameters = $(this).data("url").split("?")[1];
	if (parameters !== undefined){
		idBeer = parameters.replace("idBeer=","");
	}else {
		idBeer = $.urlParam("idBeer");
	}
	
	var url = contextPath + 'beer/findOne/' + idBeer;
    
    var formatter = new Intl.NumberFormat("pt-BR",{
		minimumFractionDigits: 1
	});
    var formatterPrice = new Intl.NumberFormat("pt-BR",{
		minimumFractionDigits: 2
	});
    
    $.ajax({
    	type: "GET",
        url: url,
        dataType: "json",
        async: false,
        success: function (result) {
        	if (result.id === -1){
        		window.location.replace(contextPath);
        		return;
        	}
        	
        	if (typeof beerList !== 'undefined'){
        		$('#backButton').html('<a href="#beerMenu" class="ui-btn" data-rel="back">Voltar</a>');        		
        	} else {
        		$('#backButton').html('<a href="beerMenu.xhtml" rel="external" class="ui-btn">Voltar</a>');        		
        	}

        	$('#beerImage').attr('src', contextPath + 'images/' + result.id );
        	$("#beerName").text('Cerveja ' + result.name + ' - ' + result.volume + 'ml');
        	$("#especificationTitle").text('Especificações');
        	$("#beerDescription").text(result.description);
        	$("#beerBrewery").html('<b>Cervejaria: </b>' + result.brewery);
        	$("#beerCountry").html('<b>País de origem: </b>' + result.countryOrigin);
        	$("#beerStyle").html('<b>Estilo da cerveja: </b>' + result.style.style);
        	$("#beerGraduation").html('<b>Graduação alcoólica: </b>' + formatter.format(result.alcoholicGraduation) + '% ABV');
        	var temperature = result.temperature !== null ? result.temperature : '';
        	$("#beerTemperature").html('<b>Temperatura de serviço: </b>' + temperature);
        	var cupType = result.cupType !== null ? result.cupType.cupType : '';
        	$("#beerCup").html('<b>Copo ideal: </b>' + cupType);
        	$("#beerBitterness").html('<b>Amargor: </b>' + result.bitterness);
        	if (!result.harmonization){
        		$("#harmonizationTitle").text('');
        	} else {
        		$("#harmonizationTitle").text('Harmonização');
        	}
        	$("#beerHarmonization").text(result.harmonization);
        	$("#beerPrice").html('<b>Valor: </b><b style="color:#FF3300 !important; font-size: 1.5em"</b>R$ ' + formatterPrice.format(result.price));
        	
        	var rateAppearanceCount = result.rate !== null && result.rate.appearanceCount !== null ? '(' + result.rate.appearanceCount + ')' : '';
        	var rateAppearanceScore = result.rate !== null && result.rate.appearanceScore !== null ? result.rate.appearanceScore.toFixed(2) : '' ;
        	var rateAromaCount = result.rate !== null && result.rate.aromaCount !== null ? '(' + result.rate.aromaCount + ')' : '';
        	var rateAromaScore = result.rate !== null && result.rate.aromaScore !== null ? result.rate.aromaScore.toFixed(2) : '';
        	var rateFlavorCount = result.rate !== null && result.rate.flavorCount !== null ? '(' + result.rate.flavorCount + ')' : '';
        	var rateFlavorScore = result.rate !== null && result.rate.flavorScore !== null ? result.rate.flavorScore.toFixed(2) : '';
        	
        	$("#beerRateAppearance").html('<b class="col-lg-2 col-xs-5">Aparência: </b><spam id="rateAppearance"></spam> ' + rateAppearanceScore + ' ' + rateAppearanceCount  );
        	$("#beerRateAroma").html('<b class="col-lg-2 col-xs-5">Aroma: </b><spam id="rateAroma"></spam> ' + rateAromaScore + ' ' + rateAromaCount  );
        	$("#beerRateFlavor").html('<b class="col-lg-2 col-xs-5">Sabor: </b><spam id="rateFlavor"></spam> ' + rateFlavorScore + ' ' + rateFlavorCount );

        	$('#rateAppearance').raty({
        		hints: ['', '', '', '', ''],
        		noRatedMsg : '',
        		score: function() {
                	return rateAppearanceScore;
        		},
        		click: function(score, evt) {
        			$('#rateAppearance').raty('readOnly', true);
        			  var url = contextPath + 'beer/rate/' + idBeer + "/appearance/" + score;
        			
        			  $.ajax({
        			    	type: "GET",
        			        url: url,
        			        dataType: "json",
        			        async: true,
        			        success: function (result) {
        			        },
        			        error: function (request, sts, error) {
        			        	console.log(error + " : " + sts);
        			            console.log('Network error has occurred please try again!');
        			        }
        			    });      
        		}
    		});
        	
        	$('#rateAroma').raty({
        		hints: ['', '', '', '', ''],
        		noRatedMsg : '',
        		score: function() {
                	return rateAromaScore;
        		},
        		click: function(score, evt) {
        			$('#rateAroma').raty('readOnly', true);
        			  var url = contextPath + 'beer/rate/' + idBeer + "/aroma/" + score;
        			
        			  $.ajax({
        			    	type: "GET",
        			        url: url,
        			        dataType: "json",
        			        async: true,
        			        success: function (result) {
        			        },
        			        error: function (request, sts, error) {
        			        	console.log(error + " : " + sts);
        			            console.log('Network error has occurred please try again!');
        			        }
        			    });      
        		}
            });
            $('#rateFlavor').raty({
            	hints: ['', '', '', '', ''],
        		noRatedMsg : '',
        		score: function() {
                	return rateFlavorScore;
        		},
        		click: function(score, evt) {
        			$('#rateFlavor').raty('readOnly', true);
        			  var url = contextPath + 'beer/rate/' + idBeer + "/flavor/" + score;
        			
        			  $.ajax({
        			    	type: "GET",
        			        url: url,
        			        dataType: "json",
        			        async: true,
        			        success: function (result) {
        			        },
        			        error: function (request, sts, error) {
        			        	console.log(error + " : " + sts);
        			            console.log('Network error has occurred please try again!');
        			        }
        			    });      
        		}
            });
        },
        error: function (request, sts, error) {
        	console.log(error + " : " + sts);
            console.log('Network error has occurred please try again!');
        }
    });         
});


$.urlParam = function(name){
	var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
};

$(document).ready(function($) {

	  if (window.history && window.history.pushState) {

	    $(window).on('popstate', function() {
	    	if (typeof beerList === 'undefined'){
	    		window.location.replace(contextPath);
        		return;
	    	}
	    });

	  }
	});

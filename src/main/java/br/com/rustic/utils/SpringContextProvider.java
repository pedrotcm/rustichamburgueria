package br.com.rustic.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContextProvider implements ApplicationContextAware {

	private static ApplicationContext context;

	@Override
	public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException {
		SpringContextProvider.setContext( applicationContext );
	}

	public static ApplicationContext getContext() {
		return context;
	}

	private static void setContext( ApplicationContext context ) {
		SpringContextProvider.context = context;
	}

}

package br.com.rustic.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

public class Utils {

	@SuppressWarnings( "unchecked" )
	public static <E> Class<E> getEntityClass( Class<E> klass ) {
		if ( HibernateProxy.class.isAssignableFrom( klass ) ) {
			return (Class<E>) klass.getSuperclass();
		}
		return klass;
	}

	@Transactional
	public static Object loadLazy( Object obj ) {
		try {
			Class<?> klass = obj.getClass();
			for ( Field field : klass.getDeclaredFields() ) {
				field.setAccessible( true );
				for ( Annotation annotation : field.getDeclaredAnnotations() ) {
					if ( annotation.annotationType().equals( ManyToOne.class ) || annotation.annotationType().equals( OneToMany.class ) || annotation.annotationType().equals( OneToOne.class ) ) {
						for ( Method method : annotation.annotationType().getDeclaredMethods() ) {
							if ( method.getName().equals( "fetch" ) && method.invoke( annotation ).equals( FetchType.LAZY ) ) {
								Object result = field.get( obj );
								Hibernate.initialize( result );
								if ( result instanceof HibernateProxy ) {
									result = ( (HibernateProxy) result ).getHibernateLazyInitializer().getImplementation();
								}
								field.set( obj, result );
								if ( result != null )
									loadLazy( field );
								break;
							}
						}
						break;
					}
				}
			}
		} catch ( SecurityException e ) {
			e.printStackTrace();
		} catch ( IllegalAccessException e ) {
			e.printStackTrace();
		} catch ( IllegalArgumentException e ) {
			e.printStackTrace();
		} catch ( InvocationTargetException e ) {
			e.printStackTrace();
		}
		return obj;
	}

}
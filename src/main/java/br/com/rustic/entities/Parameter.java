package br.com.rustic.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "PARAMETRO" )
public class Parameter extends BaseEntity {

	private static final long serialVersionUID = -5564100716362070813L;

	@Id
	@GeneratedValue
	@Column( name = "PK_ID_PARAMETRO" )
	private Integer id;

	@NotNull
	@Column( name = "DC_CHAVE", unique = true )
	private String key;

	@NotNull
	@Column( name = "DC_VALOR" )
	private String value;

	public Parameter() {}

	public Parameter( String key, String value ) {
		this.key = key;
		this.value = value;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey( String key ) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue( String value ) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( key == null ) ? 0 : key.hashCode() );
		return result;
	}

}

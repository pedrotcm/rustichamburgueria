package br.com.rustic.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table( name = "PERFIL" )
public class Profile extends BaseEntity implements GrantedAuthority {

	private static final long serialVersionUID = 937707800369961695L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_PERFIL" )
	private Integer id;

	@NotBlank
	@Column( name = "DC_FUNCAO", unique = true )
	private String role;

	@NotBlank
	@Column( name = "DC_DESCRICAO" )
	private String description;

	public Profile() {}

	public Profile( String authority ) {
		setAuthority( authority );
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public void setAuthority( String authority ) {
		this.role = authority;
	}

	@Override
	public String getAuthority() {
		return role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( role == null ) ? 0 : role.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( getClass() != obj.getClass() )
			return false;
		Profile other = (Profile) obj;
		if ( role == null ) {
			if ( other.role != null )
				return false;
		} else if ( !role.equals( other.role ) )
			return false;
		return true;
	}

	@Override
	public String toString() {
		return description + " - " + role;
	}

}
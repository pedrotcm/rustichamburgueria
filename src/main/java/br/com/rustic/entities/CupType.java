package br.com.rustic.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "TIPO_COPO" )
public class CupType extends BaseEntity {

	private static final long serialVersionUID = -2433670297186277939L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_TIPO_COPO" )
	private Integer id;

	@Column( name = "DC_TIPO_COPO" )
	private String cupType;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getCupType() {
		return cupType;
	}

	public void setCupType( String cupType ) {
		this.cupType = cupType;
	}

}

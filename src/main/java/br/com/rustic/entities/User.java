package br.com.rustic.entities;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table( name = "USUARIO" )
@XmlRootElement
public class User extends BaseEntity implements UserDetails {

	private static final long serialVersionUID = 8485404378247350057L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_USUARIO" )
	private Integer id;

	@Column( name = "DC_NOME" )
	@Size( max = 100 )
	private String name;

	@Column( name = "DC_USUARIO", unique = true )
	@NotNull
	private String username;

	@Column( name = "DC_SENHA" )
	@NotNull
	private String password;

	@Temporal( TemporalType.DATE )
	@Column( name = "DT_DATA_CRIACAO" )
	private Date createdAt;


	@ManyToMany( fetch = FetchType.EAGER )
	@JoinTable( name = "USUARIO_PERFIL", joinColumns = @JoinColumn( name = "FK_ID_USUARIO" ), inverseJoinColumns = @JoinColumn( name = "FK_ID_PERFIL" ) )
	private Set<Profile> profiles;

	private Boolean enabled;

	public User() {
		this.enabled = true;
		this.createdAt = new Date();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( createdAt == null ) ? 0 : createdAt.hashCode() );
		result = prime * result + ( ( enabled == null ) ? 0 : enabled.hashCode() );
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
		result = prime * result + ( ( password == null ) ? 0 : password.hashCode() );
		result = prime * result + ( ( username == null ) ? 0 : username.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		User other = (User) obj;
		if ( createdAt == null ) {
			if ( other.createdAt != null )
				return false;
		} else if ( !createdAt.equals( other.createdAt ) )
			return false;
		if ( enabled == null ) {
			if ( other.enabled != null )
				return false;
		} else if ( !enabled.equals( other.enabled ) )
			return false;
		if ( id == null ) {
			if ( other.id != null )
				return false;
		} else if ( !id.equals( other.id ) )
			return false;
		if ( name == null ) {
			if ( other.name != null )
				return false;
		} else if ( !name.equals( other.name ) )
			return false;
		if ( password == null ) {
			if ( other.password != null )
				return false;
		} else if ( !password.equals( other.password ) )
			return false;
		if ( username == null ) {
			if ( other.username != null )
				return false;
		} else if ( !username.equals( other.username ) )
			return false;
		return true;
	}

	@Override
	public Collection<Profile> getAuthorities() {
		return profiles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled( Boolean enabled ) {
		this.enabled = enabled;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt( Date createdAt ) {
		this.createdAt = createdAt;
	}

	public void setUsername( String username ) {
		this.username = username;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public void setAuthorities( Set<Profile> profiles ) {
		this.profiles = profiles;
	}

	public Set<Profile> getProfiles() {
		return (Set<Profile>) getAuthorities();
	}

}
package br.com.rustic.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table( name = "CERVEJA" )
public class Beer extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 2948830986309645512L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_CERVEJA" )
	private Integer id;

	@NotNull
	@Column( name = "DC_NOME" )
	private String name;

	@NotNull
	@Column( name = "DC_VOLUME" )
	private String volume;

	@NotNull
	@Column( name = "DC_TEOR_ALCOOLICO" )
	private String alcoholicGraduation;

	@OneToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "FK_ID_ESTILO" )
	private Style style;

	@NotNull
	@Column( name = "DC_PRECO" )
	private String price;

	@Column( name = "DC_CERVEJARIA" )
	private String brewery;

	@Column( name = "DC_PAIS_ORIGEM" )
	private String countryOrigin;

	@Column( name = "DC_TEMPERATURA" )
	private String temperature;

	@OneToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "FK_ID_TIPO_COPO" )
	private CupType cupType;

	@Column( name = "DC_CODIGO" )
	private String code;

	@Column( name = "DC_DESCRICAO" )
	@Size( max = 1000 )
	private String description;

	@Column( name = "DC_HARMONIZACAO" )
	@Size( max = 1000 )
	private String harmonization;

	@Column( name = "FL_EM_ESTOQUE" )
	private Boolean inStock;

	@Column( name = "DC_AMARGOR" )
	private String bitterness;

	@OneToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	@NotNull
	@JoinColumn( name = "FK_ID_IMAGEM_CERVEJA" )
	private UploadFile imageBeer;

	@OneToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_AVALIACAO" )
	private Rate rate;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume( String volume ) {
		this.volume = volume;
	}

	public String getAlcoholicGraduation() {
		return alcoholicGraduation;
	}

	public void setAlcoholicGraduation( String alcoholicGraduation ) {
		this.alcoholicGraduation = alcoholicGraduation;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle( Style style ) {
		this.style = style;
	}

	public String getBrewery() {
		return brewery;
	}

	public void setBrewery( String brewery ) {
		this.brewery = brewery;
	}

	public String getCountryOrigin() {
		return countryOrigin;
	}

	public void setCountryOrigin( String countryOrigin ) {
		this.countryOrigin = countryOrigin;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature( String temperature ) {
		this.temperature = temperature;
	}

	public CupType getCupType() {
		return cupType;
	}

	public void setCupType( CupType cupType ) {
		this.cupType = cupType;
	}

	public String getCode() {
		return code;
	}

	public void setCode( String code ) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getHarmonization() {
		return harmonization;
	}

	public void setHarmonization( String harmonization ) {
		this.harmonization = harmonization;
	}

	public UploadFile getImageBeer() {
		return imageBeer;
	}

	public void setImageBeer( UploadFile imageBeer ) {
		this.imageBeer = imageBeer;
	}

	public Boolean getInStock() {
		return inStock;
	}

	public void setInStock( Boolean inStock ) {
		this.inStock = inStock;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice( String price ) {
		this.price = price;
	}

	public Rate getRate() {
		return rate;
	}

	public void setRate( Rate rate ) {
		this.rate = rate;
	}

	public String getBitterness() {
		return bitterness;
	}

	public void setBitterness( String bitterness ) {
		this.bitterness = bitterness;
	}

}
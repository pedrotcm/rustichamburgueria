package br.com.rustic.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "AVALIACAO" )
public class Rate extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -4759532175702499109L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_AVALIACAO" )
	private Integer id;

	@Column( name = "NU_PONTUACAO_APARENCIA" )
	private Double appearanceScore;

	@Column( name = "NU_QUANTIDADE_APARENCIA" )
	private Integer appearanceCount;

	@Column( name = "NU_PONTUACAO_AROMA" )
	private Double aromaScore;

	@Column( name = "NU_QUANTIDADE_AROMA" )
	private Integer aromaCount;

	@Column( name = "NU_PONTUACAO_SABOR" )
	private Double flavorScore;

	@Column( name = "NU_QUANTIDADE_SABOR" )
	private Integer flavorCount;

	public Rate() {
		super();
		this.setAppearanceCount( 0 );
		this.setAppearanceScore( 0.0 );
		this.setAromaCount( 0 );
		this.setAromaScore( 0.0 );
		this.setFlavorCount( 0 );
		this.setFlavorScore( 0.0 );
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public Double getAppearanceScore() {
		return appearanceScore;
	}

	public void setAppearanceScore( Double appearanceScore ) {
		this.appearanceScore = appearanceScore;
	}

	public Integer getAppearanceCount() {
		return appearanceCount;
	}

	public void setAppearanceCount( Integer appearanceCount ) {
		this.appearanceCount = appearanceCount;
	}

	public Double getAromaScore() {
		return aromaScore;
	}

	public void setAromaScore( Double aromaScore ) {
		this.aromaScore = aromaScore;
	}

	public Integer getAromaCount() {
		return aromaCount;
	}

	public void setAromaCount( Integer aromaCount ) {
		this.aromaCount = aromaCount;
	}

	public Double getFlavorScore() {
		return flavorScore;
	}

	public void setFlavorScore( Double flavorScore ) {
		this.flavorScore = flavorScore;
	}

	public Integer getFlavorCount() {
		return flavorCount;
	}

	public void setFlavorCount( Integer flavorCount ) {
		this.flavorCount = flavorCount;
	}
}

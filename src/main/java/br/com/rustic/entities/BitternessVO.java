package br.com.rustic.entities;

import java.io.Serializable;

public class BitternessVO implements Serializable{

	private static final long serialVersionUID = -9120096417103086969L;

	private String description;

	public BitternessVO( String description ) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

}

package br.com.rustic.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table( name = "arquivo" )
public class UploadFile extends BaseEntity {

	private static final long serialVersionUID = -3349834377902628600L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_ARQUIVO" )
	private Integer id;

	@NotNull
	@Column( name = "DC_NOME" )
	private String name;

	@Size( max = 10 )
	@Column( name = "DC_EXTENSAO" )
	private String extension;

	@Size( max = 255 )
	@Column( name = "DC_CAMINHO" )
	private String path;

	@Transient
	private byte[] fileContent;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension( String extension ) {
		this.extension = extension;
	}

	public String getPath() {
		return path;
	}

	public void setPath( String path ) {
		this.path = path;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent( byte[] fileContent ) {
		this.fileContent = fileContent;
	}
	
}

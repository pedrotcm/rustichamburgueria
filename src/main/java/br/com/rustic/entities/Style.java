package br.com.rustic.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESTILO")
public class Style extends BaseEntity{

	private static final long serialVersionUID = 6375098687088697336L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "PK_ID_ESTILO" )
	private Integer id;
	
	@Column(name = "DC_ESTILO")
	private String style;
	
	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle( String style ) {
		this.style = style;
	}

	@Override
	public String toString() {
		return style;
	}

}

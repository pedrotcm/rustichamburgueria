package br.com.rustic.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import br.com.rustic.utils.Utils;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 6974159751715511155L;

	public static final String VERSION_COLUMN_NAME = "versao";

	@Version
	@Column( name = BaseEntity.VERSION_COLUMN_NAME )
	private Long version;

	public Long getVersion() {
		return version;
	}

	public void setVersion( Long version ) {
		this.version = version;
	}

	public abstract Integer getId();

	public abstract void setId( Integer id );

	@Override
	public boolean equals( Object obj ) {
		if ( obj == null )
			return false;
		if ( this == obj )
			return true;
		if ( this.getId() == null )
			return false;
		if ( !Utils.getEntityClass( getClass() ).equals( Utils.getEntityClass( obj.getClass() ) ) )
			return false;
		return getId().equals( ( (BaseEntity) obj ).getId() );
	}

}
package br.com.rustic.servlets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import br.com.rustic.entities.Beer;
import br.com.rustic.entities.UploadFile;
import br.com.rustic.services.BeerService;
import br.com.rustic.services.UploadFileService;

@Component( "imageBeerServlet" )
public class ImageBeerServlet extends HttpRequestHandlerServlet implements HttpRequestHandler {

	private static final long serialVersionUID = 6431495463549340686L;

	@Autowired
	private UploadFileService uploadFileService;

	@Autowired
	private BeerService beerService;

	@Override
	public void handleRequest( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
		sendImage( req, resp );
	}

	private void sendImage( HttpServletRequest req, HttpServletResponse resp ) throws FileNotFoundException, IOException {
		String idImageBeer = req.getPathInfo().substring( 1 );

		Beer beer = beerService.load( Integer.valueOf( idImageBeer ) );
		UploadFile imageBeer = uploadFileService.load( beer.getImageBeer().getId() );
		if ( imageBeer != null ) {

			File file = new File( imageBeer.getPath() );
			if ( file.exists() ) {
				resp.addHeader( "Content-Disposition", String.format( "filename=\"%s\"", imageBeer.getName() ) );
				// resp.addHeader( "Content-Type", "application/octet-stream" );
				// baixar
				// resp.addHeader( "Content-Type", "image/jpeg" );
				BufferedInputStream buffStream = new BufferedInputStream( new FileInputStream( file ) );
				try {
					byte[] buff = new byte[1024];
					int count;
					while ( ( count = buffStream.read( buff ) ) != -1 ) {
						resp.getOutputStream().write( buff, 0, count );
					}
				} finally {
					buffStream.close();
				}
			} else {
				resp.getOutputStream().println( "Image does not exist." );
				resp.setStatus( HttpServletResponse.SC_ACCEPTED );
			}
		} else {
			resp.getOutputStream().println( "Image not found." );
			resp.setStatus( HttpServletResponse.SC_NOT_FOUND );
		}
	}
}
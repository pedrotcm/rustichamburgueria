package br.com.rustic.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.rustic.entities.Beer;
import br.com.rustic.entities.Rate;
import br.com.rustic.services.BeerService;
import br.com.rustic.services.UploadFileService;

@Controller
@RequestMapping( "/beer" )
public class BeerWs {

	@Autowired
	private BeerService beerService;

	@Autowired
	private UploadFileService uploadFileService;

	@RequestMapping( value = "/findAllInStock", method = RequestMethod.GET )
	public @ResponseBody List<Beer> findAllInStock() {
		Page<Beer> beerList = beerService.findAllInStock( "" );
		return beerList.getContent();
	}

	@RequestMapping( value = "/findOne/{idBeer}", method = RequestMethod.GET )
	public @ResponseBody Beer findOne( @PathVariable Integer idBeer ) {
		Beer beer = beerService.load( idBeer );
		if ( beer == null ) {
			beer = new Beer();
			beer.setId( -1 );
		}
		return beer;
	}

	@RequestMapping( value = "/rate/{idBeer}/{category}/{score}", method = RequestMethod.GET )
	public @ResponseBody Beer rate( @PathVariable Integer idBeer, @PathVariable String category, @PathVariable Integer score ) {
		Beer beer = beerService.load( idBeer );
		Rate rate = beer.getRate();
		if ( rate == null ) {
			rate = new Rate();
			switch ( category ) {
				case "appearance":
					rate.setAppearanceScore( score.doubleValue() );
					rate.setAppearanceCount( 1 );
					break;
				case "aroma":
					rate.setAromaScore( score.doubleValue() );
					rate.setAromaCount( 1 );
					break;
				case "flavor":
					rate.setFlavorScore( score.doubleValue() );
					rate.setFlavorCount( 1 );
					break;
			}
			beer.setRate( rate );
		} else {
			switch ( category ) {
				case "appearance":
					rate.setAppearanceScore( rate.getAppearanceCount() == 0 ? score.doubleValue() : ( rate.getAppearanceScore() + score ) / 2 );
					rate.setAppearanceCount( rate.getAppearanceCount() + 1 );
					break;
				case "aroma":
					rate.setAromaScore( rate.getAromaCount() == 0 ? score.doubleValue() : ( rate.getAromaScore() + score ) / 2 );
					rate.setAromaCount( rate.getAromaCount() + 1 );
					break;
				case "flavor":
					rate.setFlavorScore( rate.getFlavorCount() == 0 ? score.doubleValue() : ( rate.getFlavorScore() + score ) / 2 );
					rate.setFlavorCount( rate.getFlavorCount() + 1 );
					break;
			}

		}
		beerService.save( beer );
		return beer;
	}
}

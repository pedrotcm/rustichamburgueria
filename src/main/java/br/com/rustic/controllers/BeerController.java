package br.com.rustic.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.rustic.entities.Beer;
import br.com.rustic.entities.CupType;
import br.com.rustic.entities.Style;
import br.com.rustic.entities.UploadFile;
import br.com.rustic.services.BeerService;
import br.com.rustic.services.CupTypeService;
import br.com.rustic.services.StyleService;

@Named
@Scope( "view" )
public class BeerController implements Serializable {

	private static final long serialVersionUID = -3303391814185815178L;

	@Autowired
	private BeerService beerService;

	@Autowired
	private StyleService styleService;

	@Autowired
	private CupTypeService cupTypeService;

	@Autowired
	private ImageController imageController;

	private Beer entity;

	private Integer startTemperature;
	private Integer endTemperature;

	@PostConstruct
	public void init() {
		clean();
	}

	public void clean() {
		entity = new Beer();
		entity.setInStock( true );
		startTemperature = 0;
		endTemperature = 0;
	}

	public void create() {

		if ( entity.getImageBeer() == null ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Imagem é obrigatório.", "Imagem é obrigatório." ) );
			return;
		}

		if ( startTemperature != 0 && endTemperature != 0 ) {
			if ( startTemperature == endTemperature ) {
				entity.setTemperature( String.format( "%d °C", startTemperature ) );
			} else {
				entity.setTemperature( String.format( "%d-%d °C", startTemperature, endTemperature ) );
			}
		}
		beerService.save( entity );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Cadastro realizado com sucesso" ) );
		clean();
	}

	public Beer getEntity() {
		return entity;
	}

	public void setEntity( Beer entity ) {
		this.entity = entity;
	}

	public List<Style> autocompleteStyle( String query ) {
		return styleService.autoCompleteStyle( query );
	}

	public List<CupType> autocompleteCupType( String query ) {
		return cupTypeService.autoCompleteCupType( query );
	}

	public List<String> autocompleteBitterness() {
		List<String> bitterness = new ArrayList<String>();
		bitterness.add( "Muito suave" );
		bitterness.add( "Suave" );
		bitterness.add( "Regular" );
		bitterness.add( "Forte" );
		bitterness.add( "Muito forte" );
		return bitterness;
	}

	public void handleFileUpload( FileUploadEvent event ) throws IOException, InterruptedException {
		UploadedFile file = event.getFile();

		if ( !Arrays.asList( "jpg", "jpe", "jpge", "png" ).contains( getExtension( file.getFileName() ).toLowerCase() ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Somente são aceitas imagens do tipo JPG, JPE, JPGE, PNG.", "Somente são aceitas imagens do tipo JPG, JPE, JPGE, PNG." ) );
			return;
		}

		UploadFile imageBeer = new UploadFile();
		imageBeer.setName( file.getFileName() );
		imageBeer.setExtension( getExtension( file.getFileName() ) );
		imageBeer.setPath( null );
		imageBeer.setFileContent( file.getContents() );

		entity.setImageBeer( imageBeer );
		imageController.setImage( file );
	}

	private String getExtension( String name ) {
		return name.replaceAll( ".+\\.(.+)", "$1" );
	}

	public Integer getStartTemperature() {
		return startTemperature;
	}

	public void setStartTemperature( Integer startTemperature ) {
		this.startTemperature = startTemperature;
	}

	public Integer getEndTemperature() {
		return endTemperature;
	}

	public void setEndTemperature( Integer endTemperature ) {
		this.endTemperature = endTemperature;
	}

}

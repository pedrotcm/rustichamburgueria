package br.com.rustic.controllers.search;

import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.component.tabview.Tab;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;

import br.com.rustic.controllers.ImageController;
import br.com.rustic.entities.Beer;
import br.com.rustic.entities.UploadFile;
import br.com.rustic.services.BeerService;

@Named
@Scope( "view" )
public class BeerSearchController extends BaseSearchController<Beer> {

	private static final long serialVersionUID = -2718835236454340861L;

	@Autowired
	private BeerService beerService;

	@Autowired
	private ImageController imageController;

	private transient Tab searchTab;
	private String searchValue;
	private Integer startTemperature;
	private Integer endTemperature;

	public BeerSearchController() {
		super( Beer.class );
	}

	@PostConstruct
	public void init() {
		this.setEditEntity( new Beer() );
		this.setRemoveEntity( new Beer() );
		clean();
	}

	public void clean() {
		this.setSearchEntity( new Beer() );
		this.searchValue = null;
		this.startTemperature = 0;
		this.endTemperature = 0;
	}

	public void handleFileUpload( FileUploadEvent event ) {
		UploadedFile file = event.getFile();

		if ( !Arrays.asList( "jpg", "jpe", "jpge", "png" ).contains( getExtension( file.getFileName() ).toLowerCase() ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Somente são aceitas imagens do tipo JPG, JPE, JPGE, PNG.", "Somente são aceitas imagens do tipo JPG, JPE, JPGE, PNG." ) );
			return;
		}

		UploadFile imageBeer = new UploadFile();
		imageBeer.setName( file.getFileName() );
		imageBeer.setExtension( getExtension( file.getFileName() ) );
		imageBeer.setPath( null );
		imageBeer.setFileContent( file.getContents() );

		getEditEntity().setImageBeer( imageBeer );

		imageController.setImage( file );
	}

	private String getExtension( String name ) {
		return name.replaceAll( ".+\\.(.+)", "$1" );
	}

	@Override
	public void edit() {
		startTemperature = 0;
		endTemperature = 0;
		if ( !StringUtils.isEmpty( getEditEntity().getTemperature() ) ) {
			String[] temperature = getEditEntity().getTemperature().split( "-" );
			if ( temperature.length > 1 ) {
				startTemperature = Integer.valueOf( temperature[0].trim() );
				endTemperature = Integer.valueOf( temperature[1].replace( "°C", "" ).trim() );
			} else {
				startTemperature = Integer.valueOf( temperature[0].replace( "°C", "" ).trim() );
				endTemperature = Integer.valueOf( temperature[0].replace( "°C", "" ).trim() );
			}
		}
		super.edit();
	}

	public void remove() {
		beerService.remove( getRemoveEntity() );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Cerveja removida com sucesso" ) );
	}

	public void update() {
		if ( startTemperature != 0 && endTemperature != 0 ) {
			if ( startTemperature == endTemperature ) {
				getEditEntity().setTemperature( String.format( "%d °C", startTemperature ) );
			} else {
				getEditEntity().setTemperature( String.format( "%d-%d °C", startTemperature, endTemperature ) );
			}
		}
		beerService.save( getEditEntity() );
		closeTab( getEditTab() );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Cerveja atualizada com sucesso" ) );
	}

	public Tab getSearchTab() {
		return searchTab;
	}

	public void setSearchTab( Tab searchTab ) {
		this.searchTab = searchTab;
	}

	@Override
	protected Page<Beer> getList() {
		return beerService.findAll( searchValue );
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue( String searchValue ) {
		this.searchValue = searchValue;
	}

	public Integer getStartTemperature() {
		return startTemperature;
	}

	public void setStartTemperature( Integer startTemperature ) {
		this.startTemperature = startTemperature;
	}

	public Integer getEndTemperature() {
		return endTemperature;
	}

	public void setEndTemperature( Integer endTemperature ) {
		this.endTemperature = endTemperature;
	}
}

package br.com.rustic.controllers.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import br.com.rustic.utils.LazySorter;

public abstract class BaseSearchController<T> implements Serializable {

	private static final long serialVersionUID = 9120846865626658993L;

	private T searchEntity;

	private T editEntity;

	private T showEntity;

	private T removeEntity;

	private final Class<T> searchEntityClass;

	private LazyDataModel<T> dataModel;

	private DataTable searchResults;

	private Tab showTab;

	private Tab editTab;

	private Tab cancelTab;

	public BaseSearchController( Class<T> searchEntityClass ) {
		this.searchEntityClass = searchEntityClass;
		newSearchEntity();
	}

	public void newSearchEntity() {
		try {
			searchEntity = searchEntityClass.newInstance();
		} catch ( Exception e ) {
			// FIXME: handle exception
		}
	}

	public T getSearchEntity() {
		return searchEntity;
	}

	public void setSearchEntity( T searchEntity ) {
		this.searchEntity = searchEntity;
	}

	public T getEditEntity() {
		return editEntity;
	}

	public void setEditEntity( T editEntity ) {
		this.editEntity = editEntity;
	}

	public T getShowEntity() {
		return showEntity;
	}

	public void setShowEntity( T showEntity ) {
		this.showEntity = showEntity;
	}

	public T getRemoveEntity() {
		return removeEntity;
	}

	public void setRemoveEntity( T removeEntity ) {
		this.removeEntity = removeEntity;
	}

	public DataTable getSearchResults() {
		return searchResults;
	}

	private void setUpDataTable( DataTable table ) {
		table.setLazy( true );
		table.setPaginator( true );
		table.setPaginatorPosition( "bottom" );
		table.setEmptyMessage( "Nenhum registro encontrado" );
		table.setRows( 10 );
		table.setVar( "item" );
		table.setRowsPerPageTemplate( "10, 15, 20, 25, 30" );
		table.setPaginatorTemplate( "{FirstPageLink} {PreviousPageLink} {CurrentPageReport} {NextPageLink} {LastPageLink}" );
		table.setCurrentPageReportTemplate( "{currentPage} de {totalPages}" );
	}

	public void onTabClose( TabCloseEvent event ) {
		closeTab( event.getTab() );
	}

	public void closeTab( Tab tab ) {
		TabView parent = (TabView) tab.getParent();
		tab.setRendered( false );
		parent.setActiveIndex( 0 );
		complementOnTabClose( tab );
	}

	public void complementOnTabClose( Tab tab ) {
		try {
			if ( tab.equals( showTab ) ) {
				showEntity = searchEntityClass.newInstance();
			}

			if ( tab.equals( editTab ) ) {
				editEntity = searchEntityClass.newInstance();
				RequestContext.getCurrentInstance().reset( ":" + tab.getParent().getId() + ":editForm" );
				RequestContext.getCurrentInstance().update( tab.getParent().getId() );
			}
		} catch ( InstantiationException e ) {
			e.printStackTrace();
		} catch ( IllegalAccessException e ) {
			e.printStackTrace();
		}
	}

	public void setSearchResults( DataTable searchResults ) {
		this.searchResults = searchResults;
		setUpDataTable( this.searchResults );
	}

	public LazyDataModel<T> getDataModel() {
		return dataModel;
	}

	public void setDataModel( LazyDataModel<T> dataModel ) {
		this.dataModel = dataModel;
	}

	public void find() {
		loadModel();
		DataTable results = this.getSearchResults();
		if ( results != null )
			results.setFirst( 0 );
	}

	private void loadModel() {
		setDataModel( new LazyDataModel<T>() {

			private static final long serialVersionUID = -7141739005508115863L;

			@Override
			public java.util.List<T> load( int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String, Object> filters ) {
				Sort sort = null;
				if ( sortField != null && !sortField.isEmpty() ) {
					sort = new Sort( new Order( sortOrder == SortOrder.DESCENDING ? Direction.DESC : Direction.ASC, sortField ) );
				}

				Page<T> results = getList();
				this.setRowCount( (int) results.getTotalElements() );

				// sort
				List<T> resultsList = new ArrayList<T>( results.getContent() );
				if ( sortField != null ) {
					Collections.sort( resultsList, new LazySorter( sortField, sortOrder ) );
				}

				// rowCount
				int dataSize = (int) results.getTotalElements();
				this.setRowCount( dataSize );

				// paginate
				if ( dataSize > pageSize ) {
					try {
						return resultsList.subList( first, first + pageSize );
					} catch ( IndexOutOfBoundsException e ) {
						return resultsList.subList( first, first + ( dataSize % pageSize ) );
					}
				} else {
					return resultsList;
				}
			};

		} );
	}

	public void edit() {
		TabView parent = (TabView) editTab.getParent();
		parent.setActiveIndex( 1 );
		editTab.setRendered( true );
	}

	public void show() {
		TabView parent = (TabView) showTab.getParent();
		parent.setActiveIndex( verificarIndex( showTab.getId() ) );
		showTab.setRendered( true );
	}

	public void loadToCancel() {
		TabView parent = (TabView) cancelTab.getParent();
		parent.setActiveIndex( verificarIndex( cancelTab.getId() ) );
		cancelTab.setRendered( true );
	}

	private int verificarIndex( String operacao ) {
		if ( operacao != null ) {
			if ( editTab != null && cancelTab != null && showTab != null ) {
				return verificarOrdemTresOperacoes( operacao );
			} else {
				return verificarOrdemDuasOperacoes( operacao );
			}
		} else {
			return 0;
		}
	}

	private int verificarOrdemTresOperacoes( String operacao ) {
		if ( operacao.equals( "cancelTab" ) ) {
			if ( ( editTab.isRendered() && cancelTab.isRendered() && showTab.isRendered() ) || ( cancelTab.isRendered() && editTab.isRendered() ) || ( editTab.isRendered() ) ) {
				return 2;
			} else {
				return 1;
			}
		}

		if ( operacao.equals( "showTab" ) ) {
			if ( ( editTab.isRendered() && cancelTab.isRendered() && showTab.isRendered() ) || ( cancelTab.isRendered() && editTab.isRendered() ) ) {
				return 3;
			} else if ( cancelTab.isRendered() || editTab.isRendered() ) {
				return 2;
			} else {
				return 1;
			}
		}
		return 1;
	}

	private int verificarOrdemDuasOperacoes( String operacao ) {
		if ( operacao.equals( "cancelTab" ) ) {
			return 1;
		}

		if ( ( cancelTab != null && cancelTab.isRendered() ) || ( showTab != null && showTab.isRendered() ) ) {
			return 2;
		} else {
			return 1;
		}
	}

	public Tab getShowTab() {
		return showTab;
	}

	public void setShowTab( Tab showTab ) {
		this.showTab = showTab;
	}

	public Tab getEditTab() {
		return editTab;
	}

	public void setEditTab( Tab editTab ) {
		this.editTab = editTab;
	}

	public Tab getCancelTab() {
		return cancelTab;
	}

	public void setCancelTab( Tab cancelTab ) {
		this.cancelTab = cancelTab;
	}

	protected abstract Page<T> getList();

}

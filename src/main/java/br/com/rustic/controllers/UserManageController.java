package br.com.rustic.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.rustic.entities.User;
import br.com.rustic.services.UserService;

@Named
@Scope( "view" )
public class UserManageController implements Serializable {

	private static final long serialVersionUID = 2881576808376953180L;

	@Autowired
	private UserService userService;

	private User entity;

	private String password;
	private String confirmPassword;

	@PostConstruct
	public void init() {
		this.entity = userService.load( userService.loadCurrentUser().getId() );
		clean();
	}

	public void clean() {
		this.password = null;
		this.confirmPassword = null;
	}

	public void update() {

		if ( password == null || confirmPassword == null || password.length() < 8 || confirmPassword.length() < 8 ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "A senha deve ter no mínimo 8 caracteres" ) );
			return;
		} else if ( !password.equals( confirmPassword ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Confirmar senha diferente da senha" ) );
			return;
		}

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String cryptPassword = passwordEncoder.encode( password );
		getEntity().setPassword( cryptPassword );

		userService.save( getEntity() );

		init();

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Usuário atualizado com sucesso" ) );

	}

	public String cancel(){
		return "/views/admin/index.xhtml?faces-redirect=true";
	}
	public User getEntity() {
		return entity;
	}

	public void setEntity( User entity ) {
		this.entity = entity;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword( String confirmPassword ) {
		this.confirmPassword = confirmPassword;
	}

}
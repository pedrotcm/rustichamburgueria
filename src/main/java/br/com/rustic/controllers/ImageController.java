package br.com.rustic.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;

import org.apache.commons.io.FileUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

@Named
@Scope( "session" )
public class ImageController {

	private UploadedFile image;

	public StreamedContent getImageContent() {
		try {
			String imageBeerPath = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get( "imageBeerPath" );

			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			InputStream isDefault = externalContext.getResourceAsStream( "/resources/images/rustic_logo.jpg" );

			FacesContext context = FacesContext.getCurrentInstance();
			if ( context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE ) {
				return new DefaultStreamedContent( isDefault );
			}
			if ( image != null ) {
				InputStream is = new ByteArrayInputStream( image.getContents() );
				this.image = null;
				return new DefaultStreamedContent( is );
			} else if ( !StringUtils.isEmpty( imageBeerPath ) ) {
				File imageBeer = new File( imageBeerPath );
				InputStream is = new ByteArrayInputStream( FileUtils.readFileToByteArray( imageBeer ) );
				return new DefaultStreamedContent( is );
			} else {
				return new DefaultStreamedContent( isDefault );
			}
		} catch ( Exception e ) {
			return new DefaultStreamedContent();
		}
	}

	public UploadedFile getImage() {
		return image;
	}

	public void setImage( UploadedFile image ) {
		this.image = image;
	}

}

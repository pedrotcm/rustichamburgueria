package br.com.rustic.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.com.rustic.entities.UploadFile;

@FacesComponent( "uploaderComponent" )
public class UploaderController extends UINamingContainer {
	private String validFormats;
	private Pattern validFormatsPattern;
	private String forceType = null;
	private Integer maxLength;

	@PostConstruct
	private void init() {
	}

	public void handleFileUpload( FileUploadEvent event ) {
		UploadedFile diskFile = event.getFile();

		if ( !hasValidFormat( diskFile ) ) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, invalidFileTypeMessage(), null ) );
			return;
		}

		UploadFile file = new UploadFile();
		file.setName( diskFile.getFileName() );
		file.setExtension( getExtension( diskFile.getFileName() ) );
		file.setPath( null );
		file.setFileContent( diskFile.getContents() );

		getFiles().add( file );
	}

	public List<UploadFile> getFilteredValue() {
		List<UploadFile> list = new ArrayList<UploadFile>();
		if ( getFiles() != null )
			for ( UploadFile file : getFiles() ) {
				if ( !list.contains( file ) )
					list.add( file );
			}
		return list;
	}


	public void removeFile( UploadFile file ) {
		if ( getFiles() != null && getFiles().contains( file ) ) {
			getFiles().remove( file );
		}
	}

	@SuppressWarnings( "unchecked" )
	private Collection<UploadFile> getFiles() {
		return (Collection<UploadFile>) getAttribute( "value" );
	}

	public boolean isMaxLengthReached() {
		return maxLength() > 0 && getFilteredValue().size() >= maxLength();
	}

	private String getExtension( String name ) {
		return name.replaceAll( ".+\\.(.+)", "$1" );
	}

	private String invalidFileTypeMessage() {
		String format = "Somente são aceitos arquivos do tipo %s";
		if ( validFormats().length() > 1 )
			format = "Somente são aceitos arquivos dos tipos %s";
		String[] types = validFormats.split( "[\\s,]+" );
		StringBuilder builder = new StringBuilder();
		for ( int i = 0; i < types.length; i++ ) {
			if ( i > 0 )
				builder.append( i == types.length - 1 ? " ou " : ", " );
			builder.append( "." );
			builder.append( types[i] );
		}
		return String.format( format, builder.toString() );
	}

	private boolean hasValidFormat( UploadedFile diskFile ) {
		if ( validFormats().equals( "*" ) )
			return true;
		Matcher matcher = getValidFormatsPattern().matcher( diskFile.getFileName() );
		return matcher.find();
	}

	private String validFormats() {
		if ( validFormats == null )
			validFormats = (String) getAttribute( "validFormats" );
		return validFormats;
	}

	private String forceType() {
		if ( forceType == null )
			forceType = (String) getAttribute( "forceType" );
		return forceType;
	}

	private Pattern getValidFormatsPattern() {
		if ( validFormatsPattern == null ) {
			String pattern = validFormats.replaceAll( "[\\s,]+", "|" );
			validFormatsPattern = Pattern.compile( String.format( "(?i)\\.(%s)$", pattern ) );
		}
		return validFormatsPattern;
	}

	private int maxLength() {
		if ( maxLength == null )
			maxLength = Integer.valueOf( String.valueOf( getAttribute( "maxLength" ) ) );
		return maxLength;
	}

	private Object getAttribute( String attribute ) {
		Map<String, Object> attrs = getAttributes();
		return attrs.get( attribute );
	}
}

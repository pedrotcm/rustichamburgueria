package br.com.rustic.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rustic.entities.User;
import br.com.rustic.enums.RoleEnum;
import br.com.rustic.repositories.UserRepository;

@Service
@Transactional
public class UserService implements Serializable{

	private static final long serialVersionUID = 3056477251712925083L;

	@Autowired
	private UserRepository userRepository;

	public User login( String username ) {
		return userRepository.login( username, RoleEnum.ADMIN.toString() );
	}

	public void save( User entity ) {
		userRepository.save( entity );
	}

	public User load( Integer userId ) {
		return userRepository.findOne( userId );
	}

	public User loadCurrentUser() {
		User user = getCurrentUser();
		return user;
	}

	private User getCurrentUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		if ( context == null )
			return null;
		Authentication authentication = context.getAuthentication();
		Object principal = authentication != null ? authentication.getPrincipal() : null;
		if ( !( principal instanceof User ) )
			return null;
		return (User) principal;
	}
}
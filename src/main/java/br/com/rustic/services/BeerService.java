package br.com.rustic.services;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.rustic.entities.Beer;
import br.com.rustic.entities.QBeer;
import br.com.rustic.repositories.BeerRepository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.OrderSpecifier;

@Service
@Transactional
public class BeerService implements Serializable{

	private static final long serialVersionUID = -8825467714709146314L;

	@Autowired
	private BeerRepository beerRepository;

	@Autowired
	private UploadFileService uploadFileService;

	private static final QBeer qBeer = QBeer.beer;

	public Beer load( Integer idBeer ) {
		return beerRepository.findOne( idBeer );
	}

	public void save( Beer entity ) {
		uploadFileService.save( entity.getImageBeer() );
		beerRepository.save( entity );
	}

	public void remove( Beer entity ) {
		beerRepository.delete( entity );
	}

	@Transactional( readOnly = true )
	public Page<Beer> findAll( String searchValue ) {
		BooleanBuilder bb = new BooleanBuilder();
		if ( !StringUtils.isEmpty( searchValue ) ) {
			bb.or( qBeer.name.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.style.style.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.countryOrigin.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.brewery.containsIgnoreCase( searchValue ) );
		}
		OrderSpecifier<String> orderByName = qBeer.name.asc();
		return new PageImpl<Beer>( (List<Beer>) beerRepository.findAll( bb, orderByName ) );
	}

	@Transactional( readOnly = true )
	public Page<Beer> findAllInStock( String searchValue ) {
		BooleanBuilder bb = new BooleanBuilder();
		if ( !StringUtils.isEmpty( searchValue ) ) {
			bb.or( qBeer.name.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.style.style.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.countryOrigin.containsIgnoreCase( searchValue ) );
			bb.or( qBeer.brewery.containsIgnoreCase( searchValue ) );
		}
		bb.and( qBeer.inStock.eq( true ) );
		OrderSpecifier<String> orderByName = qBeer.name.asc();
		return new PageImpl<Beer>( (List<Beer>) beerRepository.findAll( bb, orderByName ) );
	}
}

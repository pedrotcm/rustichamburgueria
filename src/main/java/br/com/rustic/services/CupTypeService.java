package br.com.rustic.services;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rustic.entities.CupType;
import br.com.rustic.entities.QCupType;
import br.com.rustic.repositories.CupTypeRepository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.OrderSpecifier;

@Service
@Transactional
public class CupTypeService implements Serializable {

	private static final long serialVersionUID = 2983067854313149293L;

	private static final QCupType qCupType = QCupType.cupType1;

	@Autowired
	private CupTypeRepository cupTypeRepository;

	@Transactional( readOnly = true )
	public List<CupType> autoCompleteCupType( String query ) {
		BooleanBuilder bb = new BooleanBuilder();
		bb.and( qCupType.cupType.containsIgnoreCase( query ) );
		OrderSpecifier<String> orderByCupType = qCupType.cupType.asc();
		return (List<CupType>) cupTypeRepository.findAll( bb, orderByCupType );
	}
}

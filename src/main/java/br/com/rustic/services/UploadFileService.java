package br.com.rustic.services;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Formatter;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rustic.entities.UploadFile;
import br.com.rustic.repositories.ParameterRepository;
import br.com.rustic.repositories.UploadFileRepository;

@Service
@Transactional
public class UploadFileService implements Serializable {

	private static final long serialVersionUID = 8392139382492107270L;

	@Autowired
	private ParameterRepository parameterRepository;

	@Autowired
	private UploadFileRepository uploadFileRepository;

	private String filesPath;

	public UploadFile load( Integer idImageBeer ) {
		return uploadFileRepository.findOne( idImageBeer );
	}

	public void save( Collection<UploadFile> files ) {
		if ( files != null ) {
			for ( UploadFile file : files ) {
				save( file );
			}
		}
	}
	
	public void save( UploadFile uploadFile ) {
		if ( uploadFile != null && uploadFile.getFileContent() != null && uploadFile.getId() == null )
			createFile( uploadFile );
	}

	public void remove( UploadFile uploadFile ) {
		deleteFile( uploadFile );
		uploadFileRepository.delete( uploadFile );
	}

	public String getFilePath() {
		if ( filesPath == null )
			filesPath = parameterRepository.findValueByKey( "BASE_FILE_DIR" );
		return filesPath;
	}

	private void createFile( UploadFile entityFile ) {
		try {
			byte[] content = entityFile.getFileContent();
			String filename = byteArray2Hex( content );
			String directory = getFilePath() + File.separator + new SimpleDateFormat( "yyyyMMdd" ).format( new Date() );
			File diskFile = new File( directory, filename );
			FileUtils.writeByteArrayToFile( diskFile, content );
			entityFile.setPath( diskFile.getAbsolutePath() );
		} catch ( IOException e ) {
			throw new RuntimeException( e );
		} catch ( NoSuchAlgorithmException e ) {
			throw new RuntimeException( e );
		}
	}

	private void deleteFile( UploadFile uploadFile ) {
		File file = new File( uploadFile.getPath() );
		if ( file != null )
			file.delete();
	}

	private static String byteArray2Hex( final byte[] hash ) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance( "SHA-1" );
		byte[] data = md.digest( hash );
		String result = null;
		Formatter formatter = new Formatter();
		for ( byte b : data ) {
			formatter.format( "%02x", b );
		}
		result = formatter.toString();
		formatter.close();
		return result;
	}

	
}

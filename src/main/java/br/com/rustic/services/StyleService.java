package br.com.rustic.services;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rustic.entities.QStyle;
import br.com.rustic.entities.Style;
import br.com.rustic.repositories.StyleRepository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.OrderSpecifier;

@Service
@Transactional
public class StyleService implements Serializable{

	private static final long serialVersionUID = -7918432833459165772L;

	private static final QStyle qStyle = QStyle.style1;

	@Autowired
	private StyleRepository styleRepository;

	@Transactional( readOnly = true )
	public List<Style> autoCompleteStyle(String query){
		BooleanBuilder bb = new BooleanBuilder();
		bb.and( qStyle.style.containsIgnoreCase( query ) );
		OrderSpecifier<String> orderByStyle = qStyle.style.asc();
		return (List<Style>) styleRepository.findAll( bb, orderByStyle );
	}
}

package br.com.rustic.config.spring.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class AdminAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1691285764969639989L;

	public AdminAuthenticationToken( Object principal, Object credentials ) {
		super( principal, credentials );
	}
}
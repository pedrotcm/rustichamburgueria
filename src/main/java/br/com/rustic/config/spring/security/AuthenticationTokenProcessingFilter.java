package br.com.rustic.config.spring.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import br.com.rustic.services.UserService;

public class AuthenticationTokenProcessingFilter extends AbstractAuthenticationProcessingFilter {

	private static final String DEFAULT_FILTER_PROCESSES_URL = "/login.xhtml";

	public static final Logger logger = LoggerFactory.getLogger( AuthenticationTokenProcessingFilter.class );

	@Autowired
	private UserService userService;

	public AuthenticationTokenProcessingFilter() {
		super( DEFAULT_FILTER_PROCESSES_URL );
	}

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		if ( req.getRequestURI().equals( req.getContextPath() + "/admin" ) || req.getRequestURI().equals( req.getContextPath() + "/admin/login.xhtml" ) ) {
			Authentication authResult = SecurityContextHolder.getContext().getAuthentication();
			if ( authResult != null && authResult.isAuthenticated() ) {
				// successfulAuthentication( (HttpServletRequest) request,
				// (HttpServletResponse) response, chain, authResult );
				resp.sendRedirect( req.getContextPath() + "/views/admin/index.xhtml?faces-redirect=true" );
				return;
			} else {
				resp.sendRedirect( req.getContextPath() + "/views/admin/login.xhtml?faces-redirect=true" );
				return;
			}
		} else if ( req.getRequestURI().equals( req.getContextPath() + "/views/admin/logout" ) ) {
			SecurityContextHolder.getContext().setAuthentication( null );
			resp.sendRedirect( req.getContextPath() + "/views/admin/login.xhtml?faces-redirect=true" );
			return;
		}
		chain.doFilter( request, response );
	}

	@Override
	public Authentication attemptAuthentication( HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException, IOException, ServletException {
		return null;
	}

}
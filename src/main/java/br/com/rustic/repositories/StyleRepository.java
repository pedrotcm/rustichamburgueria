package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import br.com.rustic.entities.Style;

public interface StyleRepository extends JpaRepository<Style, Integer>, QueryDslPredicateExecutor<Style> {

}

package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import br.com.rustic.entities.Beer;

public interface BeerRepository extends JpaRepository<Beer, Integer>, QueryDslPredicateExecutor<Beer> {

}

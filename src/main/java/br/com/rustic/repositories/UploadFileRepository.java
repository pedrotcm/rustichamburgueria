package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rustic.entities.UploadFile;

public interface UploadFileRepository extends JpaRepository<UploadFile, Integer> {

}

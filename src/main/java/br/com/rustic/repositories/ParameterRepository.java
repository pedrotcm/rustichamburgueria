package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.rustic.entities.Parameter;

public interface ParameterRepository extends JpaRepository<Parameter, Integer> {

	@Query( "Select value from Parameter where key = :key" )
	String findValueByKey( @Param( "key" ) String key );
}

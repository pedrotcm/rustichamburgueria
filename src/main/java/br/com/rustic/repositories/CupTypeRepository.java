package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import br.com.rustic.entities.CupType;

public interface CupTypeRepository extends JpaRepository<CupType, Integer>, QueryDslPredicateExecutor<CupType> {

}

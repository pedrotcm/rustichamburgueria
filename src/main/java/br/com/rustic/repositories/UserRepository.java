package br.com.rustic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.rustic.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query( "select u from User u inner join u.profiles p where u.username = :username and p.role = :role" )
	User login( @Param( "username" ) String username, @Param( "role" ) String role );

	User findUserByUsername( String username );

}
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Abbey');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Abt/Quadrupel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Altbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Amber Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Brown Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Ipa');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Lager');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Pale Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'American Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Barley Wine');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Belgian Blond Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Belgian Dubbel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Belgian Pale Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Belgian Specialty Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Belgian Tripel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Berlin White');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Berliner Weisse');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Bière Blanche');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Blanche');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Bock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Bohemian Pilsener');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Brown Porter');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Chopp');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dark American Lager');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dark Strong Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Doppelbock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dortmunder Export');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dry Beer');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dry Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dubbel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dunkelweizen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Dusseldorf Altbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Eisbock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'English Brown Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'English IPA');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'English Pale Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'ESB');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Especial Bitter');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Extra Special Bitter/Esb');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Faro');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Foreign Extra Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Fruit Beer');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'German Dunkelweizen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'German Weizen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Golden Strong Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Hefeweissbier Dunkel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Hefeweizen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Helles');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Helles Bock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Imperial Ipa');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'India Pale Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'India Pale Lager');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'IPA');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Japanese Rice Lager	');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Keller');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Kölsch');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Kristallweizen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Lambic-Gueuze');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Lambic-Fruit');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Lambic-Straight');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Lite');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Maibock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Malt Liquor');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Malzbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Märzen');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Munchner Dunkel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Munich Dunkel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Munich Helles');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Northern German Altbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Oatmeal Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Pale Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Pilsner');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Porters');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Premium');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Rauchbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Red Ale');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Robust Porter');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Russ');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Russian Imperial Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Saison');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Schwarzbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Shv');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Speciality Beer');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Standard Bitter');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Standart American Lager');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Sweet Stout');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Swickel ');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Tripel');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Vienna');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Weiss');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Weizenbock');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Weizenstarckbier');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Witbier');

INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Vienna Lager');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'Session IPA');
INSERT INTO ESTILO (PK_ID_ESTILO, VERSAO, DC_ESTILO ) VALUES (nextval('SEQ_ESTILO'),0,'German Weizenbock');


